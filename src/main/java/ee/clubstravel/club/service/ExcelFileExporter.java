package ee.clubstravel.club.service;

import ee.clubstravel.club.domain.Contract;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelFileExporter {
    public static ByteArrayInputStream contractEmailsToExcel(List<Contract> contracts){
        try(Workbook workbook = new XSSFWorkbook()){
            Sheet sheet = workbook.createSheet("Contracts");

            Row row = sheet.createRow(0);
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
            headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            Cell cell = row.createCell(0);
            cell.setCellValue("Name");
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(1);
            cell.setCellValue("Email");
            cell.setCellStyle(headerCellStyle);

            for (int i = 0;i<contracts.size();i++){
                Row dataRow = sheet.createRow(i+1);
                dataRow.createCell(0).setCellValue(contracts.get(i).getName());
                dataRow.createCell(1).setCellValue(contracts.get(i).getEmail());
            }

            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayInputStream(outputStream.toByteArray());
        }catch (IOException ex){
            ex.printStackTrace();
            return null;
        }
    }
}
