package ee.clubstravel.club.controller;

import ee.clubstravel.club.domain.Contract;
import ee.clubstravel.club.repo.ContractRepo;
import ee.clubstravel.club.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
@RequestMapping("/club")
public class EditController {
    private final ContractRepo contractRepo;

    @Autowired
    public EditController(ContractRepo contractRepo) {
        this.contractRepo = contractRepo;
    }

    @GetMapping("/{contract}")
    public String editForm(
            @PathVariable Contract contract,
            Model model
    ){
        model.addAttribute("contract", contract);
        return "editContract";
    }

    @PostMapping("{contract}")
    public String editContract(
            @RequestParam("contractId") Contract contract,
            @RequestParam String phone,
            @RequestParam String email,
            @RequestParam(required = false) boolean trip,
            @RequestParam(required = false) boolean installmentPlan,
            @RequestParam(value = "firstPayment", required = false)
                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate firstPayment,
            @RequestParam(value = "secondPayment",required = false)
                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate secondPayment,
            @RequestParam(value = "thirdPayment",required = false)
                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate thirdPayment,
            @RequestParam (required = false) String firstSum,
            @RequestParam (required = false) String secondSum,
            @RequestParam (required = false) String thirdSum,
            @RequestParam String comment,
            Model model
    ){
        if(!StringUtils.isEmpty(phone)){
            contract.setPhone(phone);
        }
        if(!StringUtils.isEmpty(email)){
            contract.setEmail(email);
        }
        contract.setInstallmentPlan(installmentPlan);
        if(installmentPlan){
            if(!StringUtils.isEmpty(firstPayment)){
                if(!StringUtils.isEmpty(firstSum)){
                    contract.setFirstSum(firstSum);
                    contract.setFirstPayment(firstPayment);
                }else{
                    model.addAttribute("firstSumError", "Field can't be empty");
                }
            }
            if (!StringUtils.isEmpty(secondPayment)){
                if(!StringUtils.isEmpty(secondSum)){
                    contract.setSecondSum(secondSum);
                    contract.setSecondPayment(secondPayment);
                }else{
                    model.addAttribute("secondSumError", "Field can't be empty");
                }
            }else {
                contract.setSecondPayment(null);
            }
            if(!StringUtils.isEmpty(thirdPayment)){
                if(!StringUtils.isEmpty(thirdSum)){
                    contract.setThirdSum(thirdSum);
                    contract.setThirdPayment(thirdPayment);
                }else{
                    model.addAttribute("thirdSumError", "Field can't be empty");
                }
            }else{
                contract.setThirdPayment(null);
            }
            model.addAttribute("contract", contract);
            contract.setTrip(trip);
            contract.setComment(comment);
            contractRepo.save(contract);
            return "editContract";
        }
        contract.setTrip(trip);
        contract.setComment(comment);
        contractRepo.save(contract);

        return "redirect:/club";
    }
}
