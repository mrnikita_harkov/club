package ee.clubstravel.club.controller;

import ee.clubstravel.club.domain.Role;
import ee.clubstravel.club.domain.User;
import ee.clubstravel.club.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/club/user")
@PreAuthorize("hasAuthority('ADMIN')")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String userList(Model model){
        model.addAttribute("users",userService.findAll());
        return "userList";
    }

    @GetMapping("{user}")
    public String userEditForm(@PathVariable User user, Model model){
        model.addAttribute("user",user);
        model.addAttribute("roles", Role.values());
        return "userEdit";
    }

    @PostMapping
    public String userSave(
            @RequestParam String username,
            @RequestParam(required = false) String password,
            @RequestParam (required = false)boolean active,
            @RequestParam Map<String, String> form,
            @RequestParam("userId") User user
    ){
        if(user.getRoles().contains(Role.ADMIN)){
            active=true;
        }
        userService.saveUser(user,username,password, form,active);
        return "redirect:/club/user";
    }
}
