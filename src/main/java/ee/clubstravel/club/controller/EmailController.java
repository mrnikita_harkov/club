package ee.clubstravel.club.controller;

import ee.clubstravel.club.domain.Contract;
import ee.clubstravel.club.repo.ContractRepo;
import ee.clubstravel.club.service.EmailService;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.time.temporal.ChronoUnit.DAYS;

@Controller
public class EmailController {
    private final ContractRepo contractRepo;
    private final EmailService emailService;

    @Autowired
    public EmailController(ContractRepo contractRepo, EmailService emailService) {
        this.contractRepo = contractRepo;
        this.emailService = emailService;
    }

    @Scheduled(cron = "0 30 13 * * ?") //Every day at 15:30PM
    //@Scheduled(cron = "*/15 * * * * ?")
    public void sendEmails(){
        Iterable<Contract> contracts = contractRepo.findByInstallmentPlan(true);
        LocalDate today = LocalDate.now();
        int days = 3;
        System.out.println(" SendEmails method started at: " + new Date()+"\nCount of contracts: "+ IterableUtils.size(contracts));
        for (Contract contract: contracts) {
            try {
                if (contract.getFirstPayment()!=null) {
                    if (getFirstDays(today, contract) == days) {
                        messageTemplate(contract, contract.getFirstPayment(),contract.getFirstSum());
                        if(contract.getSecondPayment()==null) {
                            contract.setInstallmentPlan(false);
                            contractRepo.save(contract);
                        }
                    }
                }if (contract.getSecondPayment() != null) {
                    if (getSecondDays(today, contract) == days) {
                        messageTemplate(contract, contract.getSecondPayment(), contract.getSecondSum());
                        if(contract.getThirdPayment()==null){
                            contract.setInstallmentPlan(false);
                            contractRepo.save(contract);
                        }
                    }
                }if(contract.getThirdPayment()!=null){
                    if (getThirdDays(today, contract) == days) {
                        messageTemplate(contract, contract.getThirdPayment(),contract.getThirdSum());
                        contract.setInstallmentPlan(false);
                        contractRepo.save(contract);
                    }
                }

            }catch (Exception e){
                System.out.println(e);
            }
        }
    }

    private long getFirstDays(LocalDate today, Contract contract) {
        return DAYS.between(today, contract.getFirstPayment());
    }

    private long getSecondDays(LocalDate today, Contract contract) {
        return DAYS.between(today, contract.getSecondPayment());
    }
    private long getThirdDays(LocalDate today, Contract contract) {
        return DAYS.between(today, contract.getThirdPayment());
    }

    private void messageTemplate(Contract contract, LocalDate date, String sum){
        String message = String.format(
                "<body style='color: black;'>"+
                       " <div style='margin: 10px; padding: 15px;'>" +
                       "        <div>Уважаемый клиент,</div>" +
                       "        <p>Напоминаем Вам что <b><i>%s</i></b> числа у Вас следующая оплата по договору <b><i>%s</i></b> на сумму: %s€</p>" +
                       "        <p>Вы можете произвести оплату по банку:</p>" +
                       "        <ul>" +
                       "            <li>Swedbank EE442200221017216080</li>" +
                       "            <li>AS Seb BANK EE541010220272311229</li>" +
                       "            <li>AS Luminor Bank EE711700017000596219</li>" +
                       "        </ul>" +
                       "         <p>В пояснении просим указать номер договора.</p>" +
                       "        <div>" +
                       "            Также Вы можете прийти в офис и оплатить картой или наличными." +
                       "            <ul style='padding-left: 15px;'>" +
                       "                <li>Это автоматическое сообщение, пожалуйста не отвечайте на него.</li>" +
                       "                <li>Если Вы уже произвели оплату, проигнорируйте это сообщение.</li>" +
                       "            </ul>" +
                       "            Ждем вас с заказом на отдых! <span><img src='https://cdn0.iconfinder.com/data/icons/flat-travel-icons-1/48/64-512.png' style='width: 2em; height: 2em;'></span>" +
                       "        </div>" +
                       "        <div style='width: 600px; height: 10px; border-top: 2px solid black; position: absolute; margin-top: 10px;'></div>" +
                       "        <footer>" +
                       "            Clubs Travel OU <br>" +
                       "            10111 Narva mnt.7 D,korpus B, 4.korrus, Tallinn <br>" +
                       "            mob. 59817307 <br>" +
                       "            tel. 613 06 34 <br>" +
                       "            www.clubstravel.ee <br>" +
                                    "<img src='https://clubstravel.ee/wp-content/uploads/2019/03/logo-2.png'>"+
                       "        </footer>" +
                       "    </div>"+
                "</body>",
                date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                contract.getContractNumber(),
                sum
        );
        String subject = "Leping "+contract.getContractNumber();
        emailService.sendEmail(contract.getEmail(), subject, message);//Send email constructor
        System.out.println("New message for "+contract.getName()+" was sent at:"+new Date() +" for: "+contract.getEmail() );

    }
}


