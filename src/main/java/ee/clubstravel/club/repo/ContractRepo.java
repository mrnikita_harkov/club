package ee.clubstravel.club.repo;

import ee.clubstravel.club.domain.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContractRepo extends JpaRepository<Contract, Long> {
    @Query("SELECT c FROM Contract c " +
            "WHERE upper(c.name) like concat('%', upper(?1), '%') and " +
            "upper(c.phone) like concat('%', upper(?2), '%') and " +
            "upper(c.contractNumber) like concat('%', upper(?3), '%') " +
            "ORDER BY c.endDate ASC")
    List<Contract> findByFilter(String name, String phone, String contractNumber);

    @Query("SELECT c FROM Contract c " +
            "WHERE MONTH(c.creationDate)=(?1) AND " +
            "YEAR(c.creationDate)=(?2)")
    List<Contract> findByDate(int month, int year);

    List<Contract> findByContractNumber(String contractNumber);


    List<Contract> findByInstallmentPlan(boolean smt);



}
