insert into usr(id, username, password, active)
    values (1,'viktoria', '123456', true);

insert into user_role(user_id, roles)
    values (1, 'USER'), (1,'ADMIN');

CREATE extension if not exists pgcrypto;

update usr set password = crypt(password, gen_salt('bf',8));