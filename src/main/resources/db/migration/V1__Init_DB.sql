create sequence hibernate_sequence start 2 increment 1;
create table contract (
    id int8 not null,
    comment varchar(2048),
    contract_number varchar(50),
    creation_date date,
    email varchar(255),
    end_date date not null,
    first_payment date,
    first_sum varchar(255),
    installment_plan boolean default false,
    name varchar(100),
    personal_code varchar(11),
    phone varchar(30),
    second_payment date,
    second_sum varchar(255),
    start_date date not null,
    third_payment date,
    third_sum varchar(255),
    trip boolean not null,
    weeks varchar(25),
    primary key (id)
);

create table user_role (
    user_id int8 not null,
    roles varchar(255)
);

create table usr (
    id int8 not null,
    active boolean not null,
    password varchar(255),
    username varchar(255),
    primary key (id)
);

alter table if exists user_role
    add constraint user_role_user_fk
    foreign key (user_id) references usr;
